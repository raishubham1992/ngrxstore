import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Store } from "@ngrx/store";
import * as loginActions from "../login/state/login.actions";
import * as dashboardActions from "../dashboard/state/dashboard.actions";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  constructor(private store: Store<any>, private router: Router) {}

  list: any;

  ngOnInit() {
    this.list = this.store.select("dashBoard");
  }

  logout() {
    this.store.dispatch(new loginActions.Logout());
    this.store.dispatch(new dashboardActions.ClearList());
    this.router.navigate(["/"]);
  }

  delete(id) {
    this.store.dispatch(new dashboardActions.DeleteItemInList(id));
  }

  edit(content: any, i: number) {
    localStorage.setItem("index", JSON.stringify(i));
    localStorage.setItem("data", JSON.stringify(content));
    this.router.navigate(["/edit"]);
  }
}
