import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';
import { DashBoardService } from '../services/dashboard.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as DashBoardActions from '../state/dashboard.actions';
import { Store } from '@ngrx/store';


@Injectable()
export class DashboardResolver implements Resolve<Observable<any>> {
  constructor(private dashBoardService: DashBoardService, private store: Store<any>) {}

  resolve() {
    return this.dashBoardService.getListService(2).pipe(map(
      (response) => {
        console.log('from',response);
        return this.store.dispatch(new DashBoardActions.GetList(response.slice(0, 5)))
      }
    ));
  }
}
