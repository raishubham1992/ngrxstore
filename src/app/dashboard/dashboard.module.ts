import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { DashBoardService } from './services/dashboard.service';
import { DashboardResolver } from './resolver/dashboard.resolver';
import { EditComponent } from './edit-component/edit.component';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    DashboardComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DashBoardService, DashboardResolver]
})
export class DashBoardModule { }
