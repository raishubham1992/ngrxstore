import {Component, OnInit} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DashBoardService } from '../services/dashboard.service';
import { Store } from '@ngrx/store';
import * as dashboardActions from '../state/dashboard.actions'



@Component({
  selector: 'edit-component',
  templateUrl: './edit.component.html',
  providers: [DashBoardService]
})
export class EditComponent implements OnInit {
  constructor(private dashboardService: DashBoardService, private store: Store<any>){

  }

  myform: FormGroup;
  data: any;
  ngOnInit(){
    this.createForm(JSON.parse(localStorage.getItem('data')));
  }

  createForm(data?){
    this.myform = new FormGroup({
      title: new FormControl(data ? data.title : ''),
      body: new FormControl(data? data.body : ''),
    });
  }

  onSubmit(){
    let index = localStorage.getItem('index')
    let formData = this.myform.value
    this.store.dispatch(new dashboardActions.EditItemInList(formData, +index))
  }
}
