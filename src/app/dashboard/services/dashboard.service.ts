import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const endpoint = 'https://jsonplaceholder.typicode.com';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class DashBoardService{
  constructor(private http: HttpClient) { }

  getListService (pageNo): Observable<any> {
    return this.http.get<any>(endpoint + '/posts', httpOptions)
  }
}
