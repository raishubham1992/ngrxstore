import * as DashBoardActions from './dashboard.actions';

export interface State {
  list: any;
}

const initialState: State = {
  list: null
};

export function dashBoardReducer(state = initialState, action: DashBoardActions.DashBoardActions) {
  switch (action.type) {
    case (DashBoardActions.GETLIST):
      return {
        ...state,
        list: action.data
      };

      case (DashBoardActions.DELETE_ITEM_IN_LIST):
      const oldList = [...state.list];
      oldList.splice(action.id, 1);
      return {
        ...state,
        list: oldList
      };

      case (DashBoardActions.EDIT_ITEM_IN_LIST):
      const singleitemFromList = state.list[action.id];
      const updatedData = {
        ...singleitemFromList,
        ...action.data
      };
      const allData = [...state.list];
      allData[action.id] = updatedData;
      return {
        ...state,
        list : allData
      }
      case (DashBoardActions.CLEAR_LIST):
      return {
        ...state,
        list : null
      }
    default:
      return state;
  }
}
