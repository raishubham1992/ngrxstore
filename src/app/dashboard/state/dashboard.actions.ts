import { Action } from '@ngrx/store';

export const GETLIST = 'GETLIST';
export const DELETE_ITEM_IN_LIST = 'DELETE_ITEM_IN_LIST';
export const EDIT_ITEM_IN_LIST = 'EDIT_ITEM_IN_LIST';
export const CLEAR_LIST = 'CLEAR_LIST';

export class GetList implements Action {
  readonly type = GETLIST;
  constructor(public data: any[]){}
}

export class DeleteItemInList implements Action {
  readonly type = DELETE_ITEM_IN_LIST;
  constructor(public id: number){}
}

export class EditItemInList implements Action {
  readonly type = EDIT_ITEM_IN_LIST;
  constructor(public data: any, public id: number){}
}

export class ClearList implements Action {
  readonly type = CLEAR_LIST;
  constructor(){}
}

export type DashBoardActions =  GetList | DeleteItemInList | EditItemInList | ClearList;
