import { ActionReducerMap } from '@ngrx/store';

import * as loginReducer from './login/state/login.reducer';
import * as dashBoardReducer from './dashboard/state/dashboard.reducer'
import { LoginEffects } from './login/state/login.effects';

export interface AppState {
  login: loginReducer.State,
  dashBoard: dashBoardReducer.State
}

export const reducers: ActionReducerMap<AppState> = {
  login: loginReducer.loginReducer,
  dashBoard: dashBoardReducer.dashBoardReducer
};

export const effects = [LoginEffects]

