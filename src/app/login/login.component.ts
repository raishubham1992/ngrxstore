import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as loginActions from './state/login.actions'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private store: Store<any>) { }

  ngOnInit() {

  }

  login(){
    let data = {
      email : 'test@test.com',
      password: '123'
    }
    this.store.dispatch(new loginActions.Signin(data))
  }

}
