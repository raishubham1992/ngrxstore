import * as LoginActions from './login.actions';

export interface State {
  token: string;
  authenticated: boolean;
}

const initialState: State = {
  token: null,
  authenticated: false
};

export function loginReducer(state = initialState, action: LoginActions.LoginActions) {
  switch (action.type) {
    case (LoginActions.SIGNIN):
      return {
        ...state,
        authenticated: true
      };

    case (LoginActions.SIGNINSUCCESS):
      return {
        ...state,
        authenticated: true,
        token: action.data.token
      };
    case (LoginActions.LOGOUT):
      return {
        ...state,
        token: null,
        authenticated: false
      };
    default:
      return state;
  }
}
