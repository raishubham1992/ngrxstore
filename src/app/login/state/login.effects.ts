import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {switchMap, map} from 'rxjs/operators';
import {Store} from '@ngrx/store';

import * as LoginActions from '../state/login.actions';
import * as loginReducer from '../state/login.reducer';
import { LoginService } from '../services/login.service';
import {Router} from "@angular/router";



@Injectable()
export class LoginEffects {

  constructor(private actions$: Actions,
    private store: Store<loginReducer.State>,
    private loginService: LoginService,
    private router: Router) {
}

  @Effect()
  login = this.actions$
    .ofType(LoginActions.SIGNIN)
    .pipe(switchMap((action: LoginActions.Signin) => {
      return this.loginService.login(action.data)
    }), map(
      (response) => {
        console.log(response);
        this.router.navigate(['/dashboard'])
        return new LoginActions.SigninSucces(response);
      }
    ));
}
