import { Action } from '@ngrx/store';

export const SIGNIN = 'SIGNIN';
export const LOGOUT = 'LOGOUT';
export const SIGNINSUCCESS = 'SIGNINSUCCESS';

export class Signin implements Action {
  readonly type = SIGNIN;
  constructor(public data: any){}
}

export class SigninSucces implements Action {
  readonly type = SIGNINSUCCESS;
  constructor(public data: any){}
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export type LoginActions =  Signin | Logout | SigninSucces
