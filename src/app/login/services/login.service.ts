import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const endpoint = 'https://reqres.in';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class LoginService{
  constructor(private http: HttpClient) { }

  login (data): Observable<any> {
    return this.http.post<any>(endpoint + '/api/login', JSON.stringify(data), httpOptions)
  }
}
